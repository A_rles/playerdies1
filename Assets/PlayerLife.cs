using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void OnTriggerEnter2D(Collider2D collisions)
    {
        Die();
        RestartLevek();
    }

    private void Die()
    {
        rb.bodyType = RigidbodyType2D.Static;
    }
    private void RestartLevek()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
